package gr.unipi.ds.msc.utils;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
/**
 * Helper class calculating the bounding box for the hotspot analysis
 */
public class Params implements Serializable {
	private static final long serialVersionUID = 203031547741399818L;
	
	public static final boolean debug = true;
	
	public static final double dayInMillis = 86400000d;	// length of a day in milliseconds
	public static final double longitudeMin = -74.25d;	// bounding box longitude min
	public static final double longitudeMax = -73.7d;	// bounding box longitude max
	public static final double latitudeMin = 40.5;		// bounding box latitude min
	public static final double latitudeMax = 40.9;		// bounding box latitude max
	//public static long dateMin = 1448920800000L;		// bounding box date min in milliseconds (2015-12-01 00:00:00)
	//public static long dateMin = 1420063200000L;		// bounding box date min in milliseconds (2015-01-01 00:00:00)
	//public static long dateMax = 1451599200000L;		// bounding box date max in milliseconds (2016-01-01 00:00:00)
	
	public long dateMin;			// bounding box date min in milliseconds
	public long dateMax;			// bounding box date max in milliseconds
	public int dateSegments; 		// number of segments in grid for z dimension (date)
	public double dateStep; 		// size of a segment in z dimension
	public int longitudeSegments; 	// number of segments in grid for x dimension (longitude)
	public int latitudeSegments; 	// number of segments in grid for y dimension (latitude)
	public double cellSize;			// size of a segment in x and y dimensions
	public long maxX;				// bounding box longitude max (segment count to max longitude)
	public long minX;				// bounding box longitude min (segment count to min longitude)
	public long maxY;				// bounding box latitude max (segment count to max latitude)
	public long minY;				// bounding box latitude min (segment count to min latitude)
	
	public transient KeyTypes keyType;
	/**
	 * Params Constructor
	 * @param dateMin Bounding box date min in milliseconds  
	 * @param dateMax Bounding box date max in milliseconds
	 * @param cellSize Cell size in degrees
	 * @param timeStepSize Step size of time (fraction of day)
	 * @param keyType Type of key used (Long key)
	 * @throws ParseException
	 */
	public Params(String dateMin, String dateMax, double cellSize, double timeStepSize, KeyTypes keyType) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.dateMax = sdf.parse(dateMax).getTime();
		this.dateMin = sdf.parse(dateMin).getTime();
		// calculate date steps and total date segments
		dateStep = dayInMillis * timeStepSize;
		dateSegments = (int) Math.ceil((this.dateMax - this.dateMin) / dateStep);

		// calculate new long and lat bounds (calculating from point with long = 0 and lat = 0)

		// calculate min and max point(x,y) values and total number of segments
		longitudeSegments = (int) Math.ceil((longitudeMax - longitudeMin) / cellSize);
		latitudeSegments = (int) Math.ceil((latitudeMax - latitudeMin) / cellSize);
		this.cellSize = cellSize;
		maxX = (long) Math.ceil(longitudeMax / cellSize);
		minX = (long) Math.floor(longitudeMin / cellSize);
		maxY = (long) Math.ceil(latitudeMax / cellSize);
		minY = (long) Math.ceil(latitudeMin / cellSize);
		
		this.keyType = keyType;
	}
}