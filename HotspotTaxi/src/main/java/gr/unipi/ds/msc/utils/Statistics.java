package gr.unipi.ds.msc.utils;

import java.io.Serializable;
/**
 * Class used for statistics calculations
 */
public class Statistics implements Serializable {
	private static final long serialVersionUID = -4963431329169308666L;
	
	public  long totalPassengerCount;	//total passenger count of grid 
	public  double passengerMean;		//passenger mean value of grid
	public  long passengerSquareSum;	//passenger square sum of grid
	public  double standardDeviation;	//standard deviation of passenger distribution on grid
	public  long n;						//total number of cells in the grid
	
	/**
	 * Statistics constructor
	 * @param sum Total passenger count of grid
	 * @param squareSum Passenger square sum of grid
	 * @param constants A Params object that has been broadcasted to nodes in the cluster
	 */
	public Statistics(long sum,long squareSum,Params constants){
		totalPassengerCount = (long)sum;
		passengerSquareSum = (long)squareSum;
		n = (constants.latitudeSegments * constants.longitudeSegments * constants.dateSegments);
		passengerMean = ((double) totalPassengerCount) / ((double)n);
		double fraction = ((double)passengerSquareSum) / ((double) n);
		standardDeviation = Math.sqrt(fraction - Math.pow(passengerMean, 2));
	}
}
