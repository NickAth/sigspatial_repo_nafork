package gr.unipi.ds.msc.utils;

/**
 * Enumeration for key types
 */
public enum KeyTypes {
	Long, String, Cell;
}
