package gr.unipi.ds.msc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.broadcast.Broadcast;

import gr.unipi.ds.msc.utils.Cell;
import gr.unipi.ds.msc.utils.Params;
import scala.Tuple2;

/**
 * Builds the cube from raw text file input
 */
public class CubeBuilder {
	/**
	 * Parses a line of an input file and creates a Cell object representing the parsed line.
	 * The population value of the Cell object represents the population of the parsed line only.
	 * If the Cell object does not belong to the bounding box, or if line argument is a header, then a null value is returned.
	 * @param line The line of input file to be parsed
	 * @param params A Params object representing the input command line arguments. Used to properly define the bounding box.
	 * @return a Cell object representing the parsed line. If the Cell object does not belong to the bounding box, a null value is returned.
	 * @throws ParseException
	 */
	private static Cell parseLineToCell(String line, Params params) throws ParseException {
		//Check whether the line is a header in order to discard it
		//The headers start with the letter 'V' as part of the word 'VendorID'
		if (!line.startsWith("V")) {
			String[] words = line.split(","); //split the csv formatted line to get individual tokens
			
			SimpleDateFormat parserSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  //used to parse dates in String format
			long time = parserSDF.parse(words[2]).getTime();  //Get the taxi's trip arrival date-time in milliseconds

			//Get the taxi's trip arrival longitude coordinate and convert it to cell x coordinate
			int x = (int) Math.abs(Math.ceil((Double.parseDouble(words[9]) / params.cellSize)) - params.maxX);
			//Get the taxi's trip arrival latitude coordinate and convert it to cell y coordinate
			int y = (int) ((Double.parseDouble(words[10]) / params.cellSize) - params.minY);
			//Convert taxi's trip arrival date time to cell z coordinate
			int z = (int) ((time - params.dateMin) / params.dateStep);
			//Get the number of passengers in the taxi
			int population = Integer.parseInt(words[3]);

			// check if values are accepted due to bounding box restrictions
			if (x >= 0 && x < params.longitudeSegments &&
				y >= 0 && y < params.latitudeSegments &&
				z >= 0 && z < params.dateSegments &&
				population > 0) /*also check that passengers existed in the taxi*/ {
				//Create a Cell object representing the parsed line and return it
				return new Cell(x, y, z, population);
			}
		}
		//Line is a header line or falls outside the bounding box
		return null;
	}
	
	/**
	 * Parses a plain RDD file to a PairRDD<Long, Integer>. Long value is the id of a Cell object and the Integer value is the population of the Cell object
	 * @param file The plain RDD file
	 * @param broadcastParams A Params object that has been broadcasted to nodes in the cluster
	 * @return a PairRDD<Long, Integer>. Long value is the id of a Cell object and the Integer value is the population of the Cell object
	 */
	public static JavaPairRDD<Long, Integer> readFileLongKey(JavaRDD<String> file, final Broadcast<Params> broadcastParams) {
		JavaPairRDD<Long, Integer> result = file.flatMapToPair(new PairFlatMapFunction<String, Long, Integer>() {
					private static final long serialVersionUID = 1L;

					public Iterable<Tuple2<Long, Integer>> call(String line) throws ParseException {
						List<Tuple2<Long, Integer>> list = new ArrayList<Tuple2<Long, Integer>>();
						Cell c = parseLineToCell(line, broadcastParams.value());
					
						if (c != null) {
							//Cell c is not a header line and falls in the defined bounding box
							list.add(new Tuple2<Long, Integer>(c.getId(broadcastParams.value()), c.getPopulation()));
						}

						// return a list of Tuple2<Long, Integer>
						return list;
					}
				});
		return result;
	}
	
	/**
	 * Helper method that returns the sum of two arguments
	 */
	private static Function2<Integer, Integer, Integer> aggregateRawGrid = new Function2<Integer, Integer, Integer>() {
		private static final long serialVersionUID = 7906560560232105882L;

		public Integer call(Integer v1, Integer v2) throws Exception {
			return v1 + v2;
		}
	};
	
	/**
	 * Aggregates the raw Cell objects according to their ids and sums their individual populations
	 * @param rawGrid the RDD of the raw Cell objects
	 * @return a new RDD with aggregated Cell objects
	 */
	public static JavaPairRDD<Long, Integer> reduceLong(JavaPairRDD<Long, Integer> rawGrid) {
		return rawGrid.reduceByKey(aggregateRawGrid).cache();
	}
}
